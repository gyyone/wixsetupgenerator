﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace WixSetupGenerator
{
    public static class WixComponent
    {
        public static string GetComponentFiles(string componentId,string varTargetDirName,List<string> pathfiles)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format(@"<Component Id=""{0}"" Guid=""{1}"">",componentId,Guid.NewGuid()));

            foreach(var filepath in pathfiles)
            {
                string filename = Path.GetFileName(filepath);
                string extension = Path.GetExtension(filepath);    
                switch (extension.ToLower())
                {
                    case ".exe":
                        sb.AppendLine("\t" + GetWixFile(varTargetDirName + filename, string.Format("$(var.{0}.TargetDir)\\{1}", varTargetDirName, filename), true));
                        break;
                    case ".dll":
                    case ".config":
                        sb.AppendLine("\t" + GetWixFile(varTargetDirName + filename, string.Format("$(var.{0}.TargetDir)\\{1}", varTargetDirName, filename), false));
                        break;
                }
              
            
            }
            sb.AppendLine("</Component>");
            return sb.ToString();
        }

        private static string GetWixFile(string id,string source,bool Iskeyfile)
        {
            return string.Format(@"<File Id = ""{0}"" Source = ""{1}"" {2}/>", id, source, Iskeyfile ? @"KeyPath=""yes""" : "");
        }
    }
}
