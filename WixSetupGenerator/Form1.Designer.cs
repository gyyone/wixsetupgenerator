﻿namespace WixSetupGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCode = new System.Windows.Forms.TextBox();
            this.txtTargetDirName = new System.Windows.Forms.TextBox();
            this.lblTargetDir = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtComponentId = new System.Windows.Forms.TextBox();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.txtBuildDir = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(2, 150);
            this.txtCode.Multiline = true;
            this.txtCode.Name = "txtCode";
            this.txtCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCode.Size = new System.Drawing.Size(665, 221);
            this.txtCode.TabIndex = 0;
            // 
            // txtTargetDirName
            // 
            this.txtTargetDirName.Location = new System.Drawing.Point(90, 6);
            this.txtTargetDirName.Name = "txtTargetDirName";
            this.txtTargetDirName.Size = new System.Drawing.Size(152, 20);
            this.txtTargetDirName.TabIndex = 1;
            // 
            // lblTargetDir
            // 
            this.lblTargetDir.AutoSize = true;
            this.lblTargetDir.Location = new System.Drawing.Point(-1, 9);
            this.lblTargetDir.Name = "lblTargetDir";
            this.lblTargetDir.Size = new System.Drawing.Size(85, 13);
            this.lblTargetDir.TabIndex = 2;
            this.lblTargetDir.Text = "Target Dir Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Component Id";
            // 
            // txtComponentId
            // 
            this.txtComponentId.Location = new System.Drawing.Point(90, 37);
            this.txtComponentId.Name = "txtComponentId";
            this.txtComponentId.Size = new System.Drawing.Size(152, 20);
            this.txtComponentId.TabIndex = 4;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(90, 99);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(125, 23);
            this.btnGenerate.TabIndex = 5;
            this.btnGenerate.Text = "Generate XML File";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // txtBuildDir
            // 
            this.txtBuildDir.Location = new System.Drawing.Point(90, 70);
            this.txtBuildDir.Name = "txtBuildDir";
            this.txtBuildDir.Size = new System.Drawing.Size(577, 20);
            this.txtBuildDir.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "build Dir";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 371);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBuildDir);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.txtComponentId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTargetDir);
            this.Controls.Add(this.txtTargetDirName);
            this.Controls.Add(this.txtCode);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.TextBox txtTargetDirName;
        private System.Windows.Forms.Label lblTargetDir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComponentId;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.TextBox txtBuildDir;
        private System.Windows.Forms.Label label2;
    }
}

