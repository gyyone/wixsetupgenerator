﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
namespace WixSetupGenerator
{
    public partial class Form1 : Form
    {
        private ConfigFile config = new ConfigFile();
        public Form1()
        {
            InitializeComponent();
        }
        

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadConfig();
        }
        private void LoadConfig()
        {
            if(File.Exists("configfile.json"))
            {
                using (StreamReader sr = new StreamReader("configfile.json"))
                {
                    config = JsonConvert.DeserializeObject<ConfigFile>(sr.ReadToEnd());
                }
            }
            txtBuildDir.Text = config.BuildDir;
            txtComponentId.Text = config.ComponentId;
            txtTargetDirName.Text = config.TargetDirName;
     
        }
        private void SaveConfig(object obj)
        {
            using (StreamWriter sw = new StreamWriter("configfile.json", false))
            {
                sw.Write(JsonConvert.SerializeObject(obj));
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveConfig(config);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            config.BuildDir = txtBuildDir.Text;
            config.TargetDirName = txtTargetDirName.Text;
            config.ComponentId = txtComponentId.Text;
            SaveConfig(config);
            txtCode.Text = WixComponent.GetComponentFiles(config.ComponentId, config.TargetDirName, Directory.GetFiles(config.BuildDir,"*.*",SearchOption.TopDirectoryOnly).ToList());
        }
    }
    public class ConfigFile
    {
        public ConfigFile()
        {

        }
        public string ComponentId { set; get; }
        public string BuildDir { set; get; }
        public string TargetDirName { set; get; }
    }
}
